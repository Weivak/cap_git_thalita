#!/usr/bin/env python
import rospy
import tf2_ros
import tf2_msgs.msg
import geometry_msgs.msg
import turtlesim.msg
import tf_conversions

rospy.init_node("sistema", anonymous=True)
rate = rospy.Rate(10)

broadcaster = tf2_ros.TransformBroadcaster()



trans = geometry_msgs.msg.TransformStamped()
trans.header.frame_id = "sol"
trans.header.stamp = rospy.Time.now()


#trans.transform.translation.x = 1
trans.transform.translation.y = 2
trans.transform.translation.z = 0

trans.transform.rotation.x = 0
trans.transform.rotation.y = 0
trans.transform.rotation.z = 0
trans.transform.rotation.w = 1

"""
def handle_turtle_pose(msg, turtlename):
    br = tf2_ros.TransformBroadcaster()
    t = geometry_msgs.msg.TransformStamped()

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "world"
    t.child_frame_id = turtlename
    t.transform.translation.x = msg.x
    t.transform.translation.y = msg.y
    t.transform.translation.z = 0.0
    q = tf_conversions.transformations.quaternion_from_euler(0, 0, msg.theta)
    t.transform.rotation.x = q[0]
    t.transform.rotation.y = q[1]
    t.transform.rotation.z = q[2]
    t.transform.rotation.w = q[3]

    br.sendTransform(t)

handle_turtle_pose

"""
while not rospy.is_shutdown():
    planetas = rospy.get_param("planetas")
    #print(planetas)
    for planeta in planetas:
      #print(planeta['name'])
      #print(planeta['orbitRadius'])
      trans.child_frame_id = planeta['name']
      trans.transform.translation.x = planeta['orbitRadius']
      broadcaster.sendTransform(trans)
      rate.sleep()

rospy.spin()  

# rospy.spin()