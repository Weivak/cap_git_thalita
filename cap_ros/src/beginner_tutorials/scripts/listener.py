import rospy
from std_msgs.msg import Int64



def callback(data):
    #rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)  # mudar essa linha pro numero que recebe
    num = data.data
    print(num)
    sub_once.unregister()



def listener():

    rospy.init_node('listener', anonymous=True)

    global sub_once
    sub_once = rospy.Subscriber('chatter', Int64, callback) # colocar print no callback

    rospy.spin()
    
    

if __name__ == '__main__':
    listener()
