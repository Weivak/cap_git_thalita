import rospy
from std_msgs.msg import Int64

pub = rospy.Publisher('chatter', Int64, queue_size=5)

def callback(data):
    num = data.data
    if num%2 != 0:
        print(num)

def talker():
    rate = rospy.Rate(10) 
    for talk in range(1,11):
        pub.publish(talk)
        rate.sleep()


def listener():
    rospy.Subscriber('chatter', Int64, callback) 

def run():
    rospy.init_node('run', anonymous=True)
    listener()
    while not rospy.is_shutdown():
        talker()


if __name__ == '__main__':
    run()
