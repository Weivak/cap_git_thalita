class Team():                    
    def __init__(self, nom, pont, cida, numJog, dinhe, titu):       
        self.nome = nom              
        self.pontuacao = pont
        self.cidade = cida
        self.numeroJogadores = numJog
        self.dinheiro = dinhe
        self.titulos = titu

    def __repr__(self):
        r = "Nome: {} Pontuação: {} Cidade: {} Quantidade Jogadores: {} Dinheiro: {} Titulos: {} \n".format(self.nome, self.pontuacao, self.cidade, self.numeroJogadores, self.dinheiro, self.titulos)
        return r

    def __gt__(self, OutraOperando):
        return self.pontuacao > OutraOperando.pontuacao

    def __add__(self, OutraOperando):
        soma = self.pontuacao + OutraOperando.pontuacao
        return soma

    def __sub__(self, OutraOperando):
        subs = self.pontuacao - OutraOperando.pontuacao
        return subs


time1 = Team("Vasco", 31, "Rio de Janeiro", 20, 20000000, 4)

time2 = Team("Flamengo", 10, "Rio de Janeiro", 22, 1000000000, 6)

time3 = Team("Cruzeiro", 25, "Belo Horizonte", 25, 40000000, 4)

time4 = Team("Palmeiras", 29, "São Paulo", 23, 50060000, 4)

teams = [time1, time2, time3, time4]

teams.sort(key=lambda Team: Team.pontuacao, reverse=True)

print(teams)

print(time1 - time2)
