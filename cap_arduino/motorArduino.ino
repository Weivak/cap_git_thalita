int pin1Motor1 = 4;
int pin2Motor1 = 5;
int pin1Motor2 = 6;
int pin2Motor2 = 7;
 
int state_temp;
int vSpeed = 200;   
char state;
 
void setup() {
  pinMode(pin1Motor1, OUTPUT);
  pinMode(pin2Motor1, OUTPUT);
  pinMode(pin1Motor2, OUTPUT);
  pinMode(pin2Motor2, OUTPUT);
 
  Serial.begin(9600);
}
 
void loop() {

  if (Serial.available() > 0) {
    state_temp = Serial.read();
    state = state_temp;
  }
 
  // Se o estado recebido for igual a '8', o carro se movimenta para frente.
  if (state == '8') {
    Serial.println("Comando para Frente");
    digitalWrite(pin1Motor1, HIGH);
    digitalWrite(pin2Motor1, LOW);
    digitalWrite(pin1Motor2, HIGH);
    digitalWrite(pin2Motor2, LOW);
    delay(2000);
  }
    // Se o estado recebido for igual a '7', o carro se movimenta para Frente Esquerda.
    else if (state == '7') {  
      Serial.println("Comando para Frente-Esquerda");
      digitalWrite(pin1Motor1, HIGH);
      digitalWrite(pin2Motor1, LOW);
      digitalWrite(pin1Motor2, HIGH);
      digitalWrite(pin2Motor2, LOW);
      delay(2000);
      digitalWrite(pin1Motor1, LOW);
      digitalWrite(pin2Motor1, LOW);
      digitalWrite(pin1Motor2, HIGH);
      digitalWrite(pin2Motor2, LOW);
      delay(2000);
  }
    // Se o estado recebido for igual a '9', o carro se movimenta para Frente Direita.
    else if (state == '9') {   
    Serial.println("Comando para Frente-Direita");
    digitalWrite(pin1Motor1, HIGH);
    digitalWrite(pin2Motor1, LOW);
    digitalWrite(pin1Motor2, HIGH);
    digitalWrite(pin2Motor2, LOW);
    delay(2000);
    digitalWrite(pin1Motor1, HIGH);
    digitalWrite(pin2Motor1, LOW);
    digitalWrite(pin1Motor2, LOW);
    digitalWrite(pin2Motor2, LOW);
    delay(2000);
  }
  // Se o estado recebido for igual a '2', o carro se movimenta para trás.
  else if (state == '2') { 
    Serial.println("Comando para Trás");
      digitalWrite(pin1Motor1, LOW);
      digitalWrite(pin2Motor1, HIGH);
      digitalWrite(pin1Motor2, LOW);
      digitalWrite(pin2Motor2, HIGH);
      delay(2000);
  }
   // Se o estado recebido for igual a '1', o carro se movimenta para Trás Esquerda.
   else if (state == '1') {  
    Serial.println("Comando para Trás-Esquerda");
      digitalWrite(pin1Motor1, LOW);
      digitalWrite(pin2Motor1, HIGH);
      digitalWrite(pin1Motor2, LOW);
      digitalWrite(pin2Motor2, HIGH);
      delay(2000);
      digitalWrite(pin1Motor1, LOW);
      digitalWrite(pin2Motor1, HIGH);
      digitalWrite(pin1Motor2, LOW);
      digitalWrite(pin2Motor2, LOW);
      delay(2000);
  }
  // Se o estado recebido for igual a '3', o carro se movimenta para Trás Direita.
  else if (state == '3') {  
    Serial.println("Comando para Trás-Direita");
      digitalWrite(pin1Motor1, LOW);
      digitalWrite(pin2Motor1, HIGH);
      digitalWrite(pin1Motor2, LOW);
      digitalWrite(pin2Motor2, HIGH);
      delay(2000);
      digitalWrite(pin1Motor1, LOW);
      digitalWrite(pin2Motor1, LOW);
      digitalWrite(pin1Motor2, LOW);
      digitalWrite(pin2Motor2, HIGH);
      delay(2000);
  }
  // Se o estado recebido for igual a '4', o carro se movimenta para esquerda.
  else if (state == '4') {   
    Serial.print("Comando para Esquerda");
      digitalWrite(pin1Motor1, LOW);
      digitalWrite(pin2Motor1, LOW);
      digitalWrite(pin1Motor2, HIGH);
      digitalWrite(pin2Motor2, LOW);
      delay(2000);
  }
  // Se o estado recebido for igual a '6', o carro se movimenta para direita.
  else if (state == '6') {   
    Serial.println("Comando para Direita");
      digitalWrite(pin1Motor1, HIGH);
      digitalWrite(pin2Motor1, LOW);
      digitalWrite(pin1Motor2, LOW);
      digitalWrite(pin2Motor2, LOW);
      delay(2000);
  }
  // Se o estado recebido for igual a '5', o carro permanece parado.
  else if (state == '5') {   
    Serial.print("Comando para Parar");
      digitalWrite(pin1Motor1, LOW);
      digitalWrite(pin2Motor1, LOW);
      digitalWrite(pin1Motor2, LOW);
      digitalWrite(pin2Motor2, LOW);
      delay(2000);
  }
}
