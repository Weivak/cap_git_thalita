import numpy as np   # import numpy
import cv2          # import opencv2
import time         # import time

# acessa o arquivo do vídeo
cap = cv2.VideoCapture('cap_visao/vpeixe.mp4')
cap2 = cv2.VideoCapture('cap_visao/vpeixe.mp4')

while(cap.isOpened()):
    # lê o vídeo
    ret, frame = cap.read()

    if ret is True:
        seg = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB) # muda cor 
        cv2.imshow('frame',seg)
    # mostra o vídeo com a alteração

    if cv2.waitKey(25) & 0xFF == ord('q'): # apertar q pra fechar
        break
  
# fecha o vídeo e fecha a janela
cap.release()
cv2.destroyAllWindows()